# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'home.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from prediction import Ui_MainWindow1


class Ui_MainWindow(object):
    def predect_window(self):
            self.window2=QtWidgets.QMainWindow()
            self.ui=Ui_MainWindow1()
            self.ui.setupUi (self.window2)
            self.window2.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(684, 497)
        MainWindow.setWindowIcon(QtGui.QIcon('/home/soumia/Desktop/plantD/icon.jpeg'))
        MainWindow.setStyleSheet("\n"
"background-color: rgb(127, 166, 113);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 661, 471))
        self.groupBox.setStyleSheet("\n"
"border:1px solid rgb(4, 4, 4);")
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.pushButton = QtWidgets.QPushButton(self.groupBox)
        self.pushButton.setGeometry(QtCore.QRect(170, 300, 90, 41))
        self.pushButton.clicked.connect(self.predect_window)
        self.pushButton.setStyleSheet("QPushButton{\n"
"    \n"
"    \n"
"    background-color: rgb(253, 253, 253);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px;\n"
"border: 2px solid ;\n"
"border-color : qconicalgradient(cx:0.398, cy:0.5625, angle:0, stop:0 rgba(97, 177, 79, 255), stop:0.213198 rgba(0, 157, 76, 255), stop:0.532995 rgba(67, 67, 67, 255), stop:0.898477 rgba(36, 161, 65, 255));\n"
"}\n"
"QPushButton::hover {\n"
"    \n"
"    background-color: rgb(127, 170, 113);\n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_2.setGeometry(QtCore.QRect(430, 300, 90, 41))
        self.pushButton_2.setStyleSheet("QPushButton{\n"
"    background-color: rgb(229, 229, 229);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px;\n"
"border: 2px solid ;\n"
"border-color : qconicalgradient(cx:0.398, cy:0.5625, angle:0, stop:0 rgba(97, 177, 79, 255), stop:0.213198 rgba(0, 157, 76, 255), stop:0.532995 rgba(67, 67, 67, 255), stop:0.898477 rgba(36, 161, 65, 255));\n"
"}\n"
"QPushButton::hover {\n"
"    \n"
"    background-color: rgb(127, 170, 113);\n"
"}")
        self.pushButton_2.setObjectName("pushButton_2")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(0, 0, 661, 471))
        self.label.setObjectName("label")
        self.label.raise_()
        self.pushButton.raise_()
        self.pushButton_2.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.actionclose = QtWidgets.QAction(MainWindow)
        self.actionclose.setObjectName("actionclose")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        MainWindow.setWhatsThis(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/img.png\"/></p></body></html>"))
        self.pushButton.setToolTip(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Samanata\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; </p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.pushButton.setText(_translate("MainWindow", "Prediction"))
        self.pushButton_2.setToolTip(_translate("MainWindow", "<html><head/><body><p>chercher une image </p><p><br/></p></body></html>"))
        self.pushButton_2.setText(_translate("MainWindow", "recherche "))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/img.png\"/></p></body></html>"))
        self.actionclose.setText(_translate("MainWindow", "close"))
   
            

import img_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
