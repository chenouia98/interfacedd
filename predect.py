import pandas as pd
import numpy as np
import cv2
import os
import mahotas
from matplotlib import pyplot as plt
import joblib
def rgb_bgr(image):
    rgb_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    return rgb_img
def bgr_hsv(rgb_img):
    hsv_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2HSV)
    return hsv_img
def img_segmentation(rgb_img,hsv_img):
    lower_green = np.array([25,0,20])
    upper_green = np.array([100,255,255])
    healthy_mask = cv2.inRange(hsv_img, lower_green, upper_green)
    result = cv2.bitwise_and(rgb_img,rgb_img, mask=healthy_mask)
    lower_brown = np.array([10,0,10])
    upper_brown = np.array([30,255,255])
    disease_mask = cv2.inRange(hsv_img, lower_brown, upper_brown)
    disease_result = cv2.bitwise_and(rgb_img, rgb_img, mask=disease_mask)
    final_mask = healthy_mask + disease_mask
    final_result = cv2.bitwise_and(rgb_img, rgb_img, mask=final_mask)
    return final_result
def fd_hu_moments(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    feature = cv2.HuMoments(cv2.moments(image)).flatten()
    return feature

def fd_haralick(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    haralick = mahotas.features.haralick(gray).mean(axis=0)
    return haralick    

def fd_histogram(image, mask=None):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hist  = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    cv2.normalize(hist, hist)
    return hist.flatten()
def segmnt_extrac(IMG_PATH):
  imgtest=[]
  x=[]
  
  imag= cv2.resize(IMG_PATH, tuple((256,256)))
  RGB_BGR= rgb_bgr(imag)
  BGR_HSV= bgr_hsv(RGB_BGR)
  IMG_SEGMENT   = img_segmentation(RGB_BGR,BGR_HSV)
  #plt.imshow(IMG_SEGMENT)

  fv_hu_moments = fd_hu_moments(IMG_SEGMENT  )
  fv_haralick   = fd_haralick(IMG_SEGMENT )
  fv_histogram  = fd_histogram(IMG_SEGMENT  )
  imgtest= np.hstack([fv_histogram,fv_haralick, fv_hu_moments])
  x.append(imgtest)
  return   x
loaded_clf = joblib.load("/home/soumia/Desktop/plantD/Plants_DR_RF.joblib")
image= cv2.imread('/home/soumia/Desktop/plantD/imageRecherche/D0/image1.JPG')
image=segmnt_extrac(image)
#img_scal=scaler.fit_transform(image)
y_pred=loaded_clf.predict(image)
print(y_pred)
