# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rechercher.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(715, 0)
        Form.setStyleSheet("background-color: rgb(127, 166, 113);")
        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 681, 491))
        self.groupBox.setStyleSheet("background-color: rgb(127, 166, 113);\n"
"border : 2px solid rgb(24, 24, 24);\n"
"border-radius : 5px\n"
"")
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.comboBox = QtWidgets.QComboBox(self.groupBox)
        self.comboBox.setGeometry(QtCore.QRect(40, 130, 221, 24))
        self.comboBox.setStyleSheet("background-color: rgb(127, 180, 113);")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.groupBox_2 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_2.setGeometry(QtCore.QRect(30, 200, 631, 271))
        self.groupBox_2.setStyleSheet("border-radius: 5px\n"
"")
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setGeometry(QtCore.QRect(10, 10, 261, 251))
        self.label_4.setText("")
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(310, 10, 261, 251))
        self.label_5.setText("")
        self.label_5.setObjectName("label_5")
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setGeometry(QtCore.QRect(0, 0, 171, 31))
        self.label_3.setStyleSheet("\n"
"background-color: rgb(127, 180, 113);\n"
"border-radius : 5px\n"
"")
        self.label_3.setObjectName("label_3")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(620, 170, 51, 51))
        self.label.setStyleSheet("border:0px")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setGeometry(QtCore.QRect(670, 0, 31, 41))
        self.label_2.setStyleSheet("border:0px\n"
"")
        self.label_2.setObjectName("label_2")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.comboBox.setItemText(0, _translate("Form", "Apple"))
        self.comboBox.setItemText(1, _translate("Form", "Tomato"))
        self.comboBox.setItemText(2, _translate("Form", "Strawberry"))
        self.comboBox.setItemText(3, _translate("Form", "Squash"))
        self.comboBox.setItemText(4, _translate("Form", "Soybean"))
        self.comboBox.setItemText(5, _translate("Form", "Raspberry"))
        self.comboBox.setItemText(6, _translate("Form", "Potato"))
        self.comboBox.setItemText(7, _translate("Form", "Pepper"))
        self.comboBox.setItemText(8, _translate("Form", "Peach"))
        self.comboBox.setItemText(9, _translate("Form", "Orange"))
        self.comboBox.setItemText(10, _translate("Form", "Grape"))
        self.comboBox.setItemText(11, _translate("Form", "Corn"))
        self.comboBox.setItemText(12, _translate("Form", "Cherry"))
        self.comboBox.setItemText(13, _translate("Form", "Blueberry"))
        self.label_3.setText(_translate("Form", "Recherecher  des image "))
        self.label.setText(_translate("Form", "<html><head/><body><p><img src=\":/image/icons8-feuille-de-chêne-50.png\"/></p></body></html>"))
        self.label_2.setText(_translate("Form", "<html><head/><body><p><img src=\":/image/icons8-feuille-26.png\"/></p></body></html>"))
import img_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
