# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'prediction2.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow1(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(700, 531)
        MainWindow.setStyleSheet("background-color: rgb(127, 170, 113);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 671, 471))
        self.groupBox.setStyleSheet("border: 1px solid rgb(12, 12, 12);\n"
"background-color: rgb(127, 166, 113);\n"
"border-radius: 6px ;")
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.groupBox_2 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_2.setGeometry(QtCore.QRect(30, 40, 611, 91))
        self.groupBox_2.setStyleSheet("background-color: rgb(127, 170, 113);\n"
"border: 1px solid rgb(12, 12, 12);\n"
"border-radius: 6px ;")
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setGeometry(QtCore.QRect(0, 0, 101, 16))
        self.label.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px")
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton.setGeometry(QtCore.QRect(70, 37, 521, 31))
        self.pushButton.setStyleSheet("QPushButton{\n"
"background-color: rgb(255, 255, 255);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px;\n"
"}\n"
"QPushButton::hover {\n"
"    \n"
"    background-color: rgb(127, 170, 113);\n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.select_image)
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(20, 20, 71, 61))
        self.label_5.setStyleSheet("border: 0px")
        self.label_5.setObjectName("label_5")
        self.label.raise_()
        self.label_5.raise_()
        self.pushButton.raise_()
        self.groupBox_5 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_5.setGeometry(QtCore.QRect(350, 160, 291, 281))
        self.groupBox_5.setStyleSheet("background-color: rgb(127, 170, 113);")
        self.groupBox_5.setTitle("")
        self.groupBox_5.setObjectName("groupBox_5")
        self.label_2 = QtWidgets.QLabel(self.groupBox_5)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 81, 21))
        self.label_2.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px")
        self.label_2.setObjectName("label_2")
        self.pushButton_2 = QtWidgets.QPushButton(self.groupBox_5)
        self.pushButton_2.setGeometry(QtCore.QRect(30, 110, 221, 28))
        self.pushButton_2.setStyleSheet("QPushButton{\n"
"background-color: rgb(255, 255, 255);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px;\n"
"}\n"
"QPushButton::hover {\n"
"    \n"
"    background-color: rgb(127, 170, 113);\n"
"}")
        self.pushButton_2.setObjectName("pushButton_2")
        self.progressBar = QtWidgets.QProgressBar(self.groupBox_5)
        self.progressBar.setGeometry(QtCore.QRect(20, 180, 251, 23))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.progressBar.setFont(font)
        self.progressBar.setAutoFillBackground(False)
        self.progressBar.setProperty("value", 70)
        self.progressBar.setOrientation(QtCore.Qt.Horizontal)
        self.progressBar.setTextDirection(QtWidgets.QProgressBar.TopToBottom)
        self.progressBar.setObjectName("progressBar")
        self.label_6 = QtWidgets.QLabel(self.groupBox)
        self.label_6.setGeometry(QtCore.QRect(10, 20, 31, 31))
        self.label_6.setStyleSheet("border : 0px\n"
"")
        self.label_6.setObjectName("label_6")
        self.groupBox_4 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 160, 321, 281))
        self.groupBox_4.setStyleSheet("background-color: rgb(127, 170, 113);")
        self.groupBox_4.setTitle("")
        self.groupBox_4.setObjectName("groupBox_4")
        self.label_4 = QtWidgets.QLabel(self.groupBox_4)
        self.label_4.setGeometry(QtCore.QRect(0, 0, 131, 21))
        self.label_4.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 10pt \"Samanata\";\n"
"border-radius: 2px")
        self.label_4.setObjectName("label_4")
        self.label_11 = QtWidgets.QLabel(self.groupBox_4)
        self.label_11.setGeometry(QtCore.QRect(10, 20, 301, 251))
        self.label_11.setText("")
        self.label_11.setObjectName("label_11")
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 41, 31))
        self.label_3.setStyleSheet("border:0px\n"
"")
        self.label_3.setObjectName("label_3")
        self.label_7 = QtWidgets.QLabel(self.groupBox)
        self.label_7.setGeometry(QtCore.QRect(340, 130, 58, 41))
        self.label_7.setStyleSheet("border:0px")
        self.label_7.setObjectName("label_7")
        self.label_9 = QtWidgets.QLabel(self.groupBox)
        self.label_9.setGeometry(QtCore.QRect(620, 10, 41, 41))
        self.label_9.setStyleSheet("border:0px")
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.groupBox)
        self.label_10.setGeometry(QtCore.QRect(310, 150, 31, 31))
        self.label_10.setStyleSheet("border:0px")
        self.label_10.setObjectName("label_10")
        self.label_8 = QtWidgets.QLabel(self.groupBox)
        self.label_8.setGeometry(QtCore.QRect(620, 140, 41, 41))
        self.label_8.setStyleSheet("border:0px")
        self.label_8.setObjectName("label_8")
        self.label_7.raise_()
        self.label_3.raise_()
        self.label_6.raise_()
        self.groupBox_5.raise_()
        self.groupBox_2.raise_()
        self.groupBox_4.raise_()
        self.label_9.raise_()
        self.label_10.raise_()
        self.label_8.raise_()
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        self.label_12.setGeometry(QtCore.QRect(320, 10, 71, 20))
        self.label_12.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_12.setAlignment(QtCore.Qt.AlignCenter)
        self.label_12.setObjectName("label_12")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "select image "))
        self.pushButton.setText(_translate("MainWindow", "Inserer votre image "))
        self.label_5.setWhatsThis(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-image-48.png\"/></p></body></html>"))
        self.label_5.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-image-48.png\"/></p></body></html>"))
        self.label_2.setText(_translate("MainWindow", "prédiction"))
        self.pushButton_2.setText(_translate("MainWindow", "prédicter"))
        self.label_6.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-cerclé-1-c-24(1).png\"/></p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "image selectionné"))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-cerclé-2-24.png\"/></p></body></html>"))
        self.label_7.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-cerclé-3-24.png\"/></p></body></html>"))
        self.label_9.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-feuille-26.png\"/></p></body></html>"))
        self.label_10.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-feuille-26.png\"/></p></body></html>"))
        self.label_8.setText(_translate("MainWindow", "<html><head/><body><p><img src=\":/image/icons8-feuille-26.png\"/></p></body></html>"))
        self.label_12.setText(_translate("MainWindow", "Prediction"))
    def select_image(self):
            url_dir=QtWidgets.QFileDialog.getOpenFileName(Ui_MainWindow1,'select image')
            pix=QtGui.QPixmap(url_dir).scaled(301,251)
            self.label_11.setPixmap(pix)
import img_rc 


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow1()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
